# armv7m

Image NetBSD 9 for Raspberry pi rpi3b, with customized added netbsd user.

![](images/raspberry-pi-rpi3b-img.png)

![](https://gitlab.com/openbsd98324/armv7m/-/raw/master/desktop.png)


# Download 

 /usr/bin/wget  -c    "https://gitlab.com/openbsd98324/armv7m/-/raw/master/v1/armv7m.img.gz" 


# Installation 

1.) NetBSD can operate with the filesystem on the MMC (ld0) as usual.


2.) Furthermore, it is also possible to have the MMC (ld0) as a boot, and
to use an USB harddisk, like SSD disk, for the filesystem (sd0).
ld0 will boot the pi. sd0a will have the filesystem on the disk. 


## Method 1.) NetBSD on MMC

zcat armv7m.img.gz > /dev/sdXXX

If you use Windows, you may use rawrite32. 



## Method 2.) MMC Boot + USB Harddisk as filesystem 

1.) Installation of boot/kernel image on SD/MMC: 

zcat image-sda-fat-netbsd-armv7cm-SD-card-mmc-bootable-sd0a.img.gz > /dev/sdXXX 


2.) Copy the system on your USB Harddisk:

zcat armv7m.img.gz > /dev/sdYYY

3.) Grow FS will be done automacally at first boot.




# Packages

login: netbsd
password: netbsd 
(in group wheel already)

 export PKG_PATH="http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.1/All/" ;   pkg_add -v pkgin ; pkgin update


Example:
 pkgin install eboard

 



Have Fun!


